package com.avaya;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.avaya.user.util.UserEntityToModelConverter;
import com.avaya.user.util.UserModelToEntityConverter;
import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class UserApplication {

  private static ApplicationContext applicationContext;

	public static void main(String[] args) {
		applicationContext = new AnnotationConfigApplicationContext(UserApplication.class);

	}
	
	@Bean
	public UserModelToEntityConverter getUserModelToEntityConverter() {
		return new UserModelToEntityConverter();
	}
	
	@Bean
	public UserEntityToModelConverter getUserEntityToModelConverter() {
		return new UserEntityToModelConverter();
	}
	
	@Bean
	public ObjectMapper objectMapper() {
		return new ObjectMapper();
	}

}
