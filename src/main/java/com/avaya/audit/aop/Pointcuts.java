package com.avaya.audit.aop;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Pointcuts {
	
	private String name;
	
	private List<String> methods;
}
