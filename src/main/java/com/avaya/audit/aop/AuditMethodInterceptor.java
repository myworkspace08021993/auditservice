package com.avaya.audit.aop;

import com.avaya.audit.service.AuditLoggingService;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public class AuditMethodInterceptor implements MethodInterceptor {

    private AuditLoggingService auditService;

    public AuditMethodInterceptor(AuditLoggingService auditService) {
        this.auditService = auditService;
    }

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {

        if (invocation == null) {
            System.err.println("Joinpoint is null");
            throw new RuntimeException("Joinpoint is null");
        }

        return auditService.processInterceptor(invocation);

    }

}