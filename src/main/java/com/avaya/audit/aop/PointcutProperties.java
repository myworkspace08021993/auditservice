package com.avaya.audit.aop;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import lombok.Getter;
import lombok.Setter;

@Configuration
@ConfigurationProperties(prefix = "audit")
@EnableConfigurationProperties(PointcutProperties.class)
@PropertySource(value = "classpath:application.properties")
@Getter
@Setter
public class PointcutProperties {
	
	List<Pointcuts> pointcuts;
	
}
