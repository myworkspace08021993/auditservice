package com.avaya.audit.jpa.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.avaya.audit.jpa.entity.AuditData;


@Repository
public interface AuditLoggingJpaRepository extends JpaRepository<AuditData, Long> {

}
