package com.avaya.audit.jpa.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.vladmihalcea.hibernate.type.json.JsonNodeBinaryType;
import com.fasterxml.jackson.databind.JsonNode;

import lombok.Data;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

@Entity
@Table(name = "audit")
@TypeDef(name = "jsonb-node", typeClass = JsonNodeBinaryType.class)
@Data
public class AuditData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "created_at")
	private Long createdAt;
	
	@Column(name = "account_id")
	private String accountId;
	
	@Column(name="audit_object_type")
	private String auditObjectType;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "operation")
	private String operation;
	
	@Column(name="source_ip")
	private String sourceIp;

  @Type(type = "jsonb-node")
  @Column(name = "data", columnDefinition = "jsonb")
	private JsonNode data;
}
