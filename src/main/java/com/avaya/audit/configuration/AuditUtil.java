package com.avaya.audit.configuration;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.avaya.audit.aop.PointcutProperties;
import com.avaya.audit.aop.Pointcuts;


@Component
public class AuditUtil {
	
	@Autowired
	private  PointcutProperties pointcutProperties;
	
	public  String evaluatePointcuts() {
		
		List<Pointcuts> pointcuts = pointcutProperties.getPointcuts();

    pointcuts.forEach(x -> System.out.println(x.getName()+" "+x.getMethods()));
		
		if (pointcuts == null || pointcuts.isEmpty()) {
			throw new RuntimeException("Pointcut value can't be empty");
		}
		
		StringBuilder sb = new StringBuilder();
		sb.append("( ");
		
		for (int i = 0; i < pointcuts.size(); i++) {
			if (i > 0) {
				sb.append(" || ");
			}
			
			Pointcuts pointcut = pointcuts.get(i);
			
			String p =preparePointCut(pointcut.getName(), pointcut.getMethods().get(0));
			sb.append(p);
			for(int j=1;j<pointcut.getMethods().size();j++) {
				sb.append(" || ");
				sb.append(preparePointCut(pointcut.getName(), pointcut.getMethods().get(j)));
			}
		}
		
		sb.append(" || ");
		sb.append("@annotation(com.avaya.audit.custom.annotations.Loggable)");
		sb.append(" )");
		sb.append(" && ");
		sb.append("!@annotation(com.avaya.audit.custom.annotations.NotLoggable)");
		
		return sb.toString();
	}

	private static String preparePointCut(String className, String method) {
		StringBuilder sb = new StringBuilder();

		sb.append("execution (* ");
		sb.append(className).append(".");
		sb.append(method);
		sb.append("(..))");

		return sb.toString();
	}
}
