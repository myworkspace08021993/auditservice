package com.avaya.audit.configuration;

import javax.annotation.PostConstruct;

import org.springframework.aop.aspectj.AspectJExpressionPointcutAdvisor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.avaya.audit.aop.AuditMethodInterceptor;
import com.avaya.audit.service.AuditLoggingService;

@Configuration
public class AspectConfiguration {

    
    private String pointcutExpression;
    
    @Autowired
    AuditUtil auditUtil;
    
    @PostConstruct
    void setPointcutExpression() {
    	pointcutExpression = auditUtil.evaluatePointcuts();
      System.out.println(pointcutExpression);
    }

	@Bean
    @ConditionalOnProperty(value="audit.aspect.enable",havingValue = "true")
    public AspectJExpressionPointcutAdvisor configurabledvisor(@Autowired AuditLoggingService auditService) {
        AspectJExpressionPointcutAdvisor advisor = new AspectJExpressionPointcutAdvisor();
        advisor.setExpression(pointcutExpression);
        advisor.setAdvice( new AuditMethodInterceptor(auditService));
        return advisor;
    }
	
}
