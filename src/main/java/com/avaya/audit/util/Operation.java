package com.avaya.audit.util;

import java.util.Arrays;
import java.util.Optional;

import lombok.Getter;

@Getter
public enum Operation {

	ADD("save"), UPDATE("update"), DELETE("delete"), GET("find");

	private String operation;

	private Operation(String operation) {
		this.operation = operation;
	}

	public static Operation getByValue(String value) {
		
		if(value==null) {
			return null;
		}
		Optional<Operation> result = Arrays.stream(values()).filter(each -> value.contains(each.getOperation()))
				.findFirst();
		
		Operation operationByValue = null;
		if(result.isPresent()) {
			operationByValue = result.get();
		}

		return operationByValue;
	}

	@Override
	public String toString() {
		return this.name();
	}
}
