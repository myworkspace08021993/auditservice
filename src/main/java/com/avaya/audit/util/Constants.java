package com.avaya.audit.util;

public class Constants {
	
	private Constants() {}

	public static final String ADMIN = "ADMIN";

	public static final String SAVE = "save";

	public static final String UPDATE = "update";
	
	public static final String ACCOUNT_ID = "accountId";

	public static final String CREATED_AT = "createdAt";
}
