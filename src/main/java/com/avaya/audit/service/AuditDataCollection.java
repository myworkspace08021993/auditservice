package com.avaya.audit.service;

import org.aopalliance.intercept.MethodInvocation;
import org.aspectj.lang.ProceedingJoinPoint;

import com.avaya.audit.jpa.entity.AuditData;

public interface AuditDataCollection {
	
	AuditData populateAuditData(MethodInvocation invocation);

	AuditData populateAuditData(ProceedingJoinPoint joinpoint);
}
