package com.avaya.audit.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.aopalliance.intercept.MethodInvocation;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.avaya.audit.jpa.entity.AuditData;
import com.avaya.audit.jpa.repo.AuditLoggingJpaRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class AuditLoggingServiceImpl implements AuditLoggingService {

	@Autowired
	AuditLoggingJpaRepository auditRepo;
	
	@Autowired
	AuditDataCollection auditDataCollection;

	@Autowired
	ObjectMapper objectMapper;

	@PersistenceContext
	EntityManager entityManager;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Object processInterceptor(MethodInvocation invocation) throws Throwable {

		// passing control to the original transaction
		Object object = invocation.proceed();

		// pass control to application to populate audit data
		AuditData auditData = auditDataCollection.populateAuditData(invocation);
		
		System.out.println("Adding audit entry...");
		// adding audit entry
		addAuditEntry(auditData);
		
		return object;

	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Object process(ProceedingJoinPoint joinpoint) throws Throwable {

		// passing control to the original transaction
		Object object = joinpoint.proceed();

		// pass control to application to populate audit data
		AuditData auditData = auditDataCollection.populateAuditData(joinpoint);

		// adding audit entry
		addAuditEntry(auditData);
		
		return object;
	}

	private AuditData addAuditEntry(AuditData auditData) {

		return auditRepo.save(auditData);
	}

	
}
