package com.avaya.audit.service;

import org.aopalliance.intercept.MethodInvocation;
import org.aspectj.lang.ProceedingJoinPoint;

public interface AuditLoggingService {

	Object process(ProceedingJoinPoint joinpoint) throws Throwable;

    Object processInterceptor(MethodInvocation invocation) throws Throwable;
}
