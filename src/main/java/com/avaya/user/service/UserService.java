package com.avaya.user.service;

import java.util.List;

import com.avaya.user.model.User;

public interface UserService {
	
	User addUser(User user);
	
	User updateUser(String id,User user);
	
	void deleteUser(String id);
	
	User getUser(String id);
	
	List<User> getAllUsers();
}
