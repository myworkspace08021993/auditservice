package com.avaya.user.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.avaya.audit.custom.annotations.Loggable;
import com.avaya.audit.custom.annotations.NotLoggable;
import com.avaya.user.jpa.repository.UserJpaRepository;
import com.avaya.user.model.User;
import com.avaya.user.util.UserEntityToModelConverter;
import com.avaya.user.util.UserModelToEntityConverter;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	UserJpaRepository repo;
	
	@Autowired
	UserModelToEntityConverter modelToEntityconverter;
	
	@Autowired
	UserEntityToModelConverter entityToModelConverter;
	
	@Override
	public User addUser(User user) {
		
		com.avaya.user.jpa.entity.User userEntity = modelToEntityconverter.convert(user);
		
		repo.save(userEntity);
		return user;
	}
	
	@Override
	public User updateUser(String id, User user) {
		
		com.avaya.user.jpa.entity.User userEntity = repo.getById(Long.valueOf(id));
		
		userEntity.setFirstName(user.getFirstName());
		userEntity.setLastName(user.getLastName());
		userEntity.setUserId(user.getUserId());
		
		repo.save(userEntity);
		
		return user;
	}

	@Override
	public void deleteUser(String id) {
		
		Optional<com.avaya.user.jpa.entity.User> userEntity = repo.findById(Long.valueOf(id));
		
		if(userEntity.isPresent()) {
			repo.delete(userEntity.get());
		}
	}

	@Override
	@Loggable
	public User getUser(String id) {
		com.avaya.user.jpa.entity.User userEntity = repo.getById(Long.valueOf(id));
		
		return entityToModelConverter.convert(userEntity);
	}

	@Override
	@NotLoggable
	public List<User> getAllUsers() {
		
		List<com.avaya.user.jpa.entity.User> users = repo.findAll();
		
		List<User> allUsers = null;
		
		if(users!=null) {
			allUsers = users.stream().map(entityToModelConverter::convert).collect(Collectors.toList());
			
		}
		
		return allUsers;
	}

}
