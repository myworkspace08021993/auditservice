package com.avaya.user.service.audit;

import com.fasterxml.jackson.databind.JsonNode;
import org.aopalliance.intercept.MethodInvocation;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.avaya.audit.jpa.entity.AuditData;
import com.avaya.audit.service.AuditDataCollection;
import com.avaya.audit.util.Operation;
import com.avaya.user.model.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class UserAuditDataCollectionImpl implements AuditDataCollection {
	
	@Autowired
	ObjectMapper objectMapper;
	
	@Override
	public AuditData populateAuditData(MethodInvocation invocation) {
		
		
		// Individual services should extract data from MethodInvocation argument and
		// populate audit data object
		
		//This is dummy data population
		
		return getAuditData();
	}

	@Override
	public AuditData populateAuditData(ProceedingJoinPoint joinpoint) {
		// Individual services should extract data from ProceedingJoinPoint argument and
		// populate audit data object

		// This is dummy data population

		return getAuditData();
	}
	
	private AuditData getAuditData() {
		AuditData auditData = new AuditData();
		
		auditData.setAccountId("BPSPJV");
		auditData.setAuditObjectType("User");
		auditData.setCreatedAt(System.currentTimeMillis());
		auditData.setCreatedBy("ADMIN");
		auditData.setOperation(Operation.ADD.toString());
		auditData.setSourceIp("122.43.54.31");
		
		
		User user = new User();
		user.setAccountId("BPSPJV");
		user.setFirstName("Test");
		user.setLastName("Test");
		user.setUserId("test@avaya.com");
		
		String jsonData = null;
		try {
			jsonData = objectMapper.writeValueAsString(user);
		} catch (JsonProcessingException e) {
			throw new RuntimeException("Failed to convert to Json");
		}
		
		auditData.setData(objectMapper.convertValue(user, JsonNode.class));
		return auditData;
	}

}
