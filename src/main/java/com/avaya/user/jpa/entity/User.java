package com.avaya.user.jpa.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "user")
@Data
public class User  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	Long id;
	
	@Column(name = "user_id")
	String userId;
	
	@Column(name = "created_at")
	Long createdAt;
	
	@Column(name = "account_id")
	String accountId;
	
	@Column(name = "first_name", nullable = false)
	String firstName;
	
	@Column(name = "last_name")
	String lastName;

}
