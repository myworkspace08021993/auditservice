package com.avaya.user.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.avaya.user.jpa.entity.User;

@Repository
public interface UserJpaRepository extends JpaRepository<User, Long> {
	
}
