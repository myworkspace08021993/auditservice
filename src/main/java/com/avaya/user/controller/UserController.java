package com.avaya.user.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.avaya.user.model.User;
import com.avaya.user.service.UserService;

@RestController
public class UserController {

	@Autowired
	UserService service;
	
	@GetMapping(value = "/users/{id}", produces = MediaType.APPLICATION_JSON_VALUE )
	public ResponseEntity<User> getUser(@PathVariable("id") String id){
		User user = service.getUser(id);
		
		return ResponseEntity.status(HttpStatus.OK).body(user);
	}
	
	@GetMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE )
	public ResponseEntity<List<User>> getUser(){
		List<User> users = service.getAllUsers();
		
		return ResponseEntity.status(HttpStatus.OK).body(users);
	}

	@PostMapping(value = "/users", consumes = MediaType.APPLICATION_JSON_VALUE, 
	        produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> addUser(@RequestBody User user) {
		User result = service.addUser(user);

		return ResponseEntity.status(HttpStatus.ACCEPTED).body(result);
	}
	
	@PutMapping(value = "/users/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, 
	        produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> updateUser(@PathVariable("id") String id,@RequestBody User user) {
		User result = service.updateUser(id, user);

		return ResponseEntity.status(HttpStatus.OK).body(result);
	}
	
	@DeleteMapping(value = "/users/{id}")
	public ResponseEntity<Void> deleteUser(@PathVariable("id") String id) {
		service.deleteUser(id);

		return ResponseEntity.status(HttpStatus.OK).build();
	}
}
