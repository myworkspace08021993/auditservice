package com.avaya.user.util;

import org.springframework.core.convert.converter.Converter;

import com.avaya.user.model.User;

public class UserModelToEntityConverter implements Converter<User, com.avaya.user.jpa.entity.User> {

	@Override
	public com.avaya.user.jpa.entity.User convert(User userModel) {
		
		if(userModel==null) {
			return null;
		}
		
		com.avaya.user.jpa.entity.User userEntity = new com.avaya.user.jpa.entity.User();
		userEntity.setUserId(userModel.getUserId());
		userEntity.setFirstName(userModel.getFirstName());
		userEntity.setLastName(userModel.getLastName());
		userEntity.setCreatedAt(System.currentTimeMillis());
		userEntity.setAccountId(userModel.getAccountId());
		
		return userEntity;
	}

}
