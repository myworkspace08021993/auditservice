package com.avaya.user.util;

import org.springframework.core.convert.converter.Converter;

import com.avaya.user.jpa.entity.User;

public class UserEntityToModelConverter implements Converter<User, com.avaya.user.model.User> {

	@Override
	public com.avaya.user.model.User convert(User userEntity) {
		
		if(userEntity==null) {
			return null;
		}
		
		com.avaya.user.model.User userModel = new com.avaya.user.model.User();
		userModel.setUserId(userEntity.getUserId());
		userModel.setFirstName(userEntity.getFirstName());
		userModel.setLastName(userEntity.getLastName());
		userModel.setAccountId(userEntity.getAccountId());
		
		return userModel;
	}

}
