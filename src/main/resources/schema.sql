CREATE TABLE user (
    id  BIGINT NOT NULL,
    user_id  VARCHAR(128),
    created_at BIGINT ,
    account_id VARCHAR(128),
    first_name VARCHAR(128) NOT NULL,
    last_name VARCHAR(128) ,
    PRIMARY KEY (id)
);


CREATE TABLE audit (
    id BIGINT NOT NULL,
    account_id VARCHAR(128) ,
    created_at BIGINT ,
    created_by VARCHAR(128) ,
    audit_object_type VARCHAR(128) ,
    opearion VARCHAR(128) ,
    source_ip VARCHAR(128) ,
    data VARCHAR(128) ,
    PRIMARY KEY (id)
);